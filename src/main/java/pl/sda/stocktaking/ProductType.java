package pl.sda.stocktaking;

public enum ProductType {
    FOOD,
    INDUSTRIAL,
    ALCOHOL;
}
