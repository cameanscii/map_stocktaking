package pl.sda.stocktaking;

import java.util.*;

public class Magazine {
    HashMap<ProductClass, List<Product>> productClassListMap=new HashMap<ProductClass, List<Product>>();
    HashMap<ProductType, List<Product>> productTypeListMap=new HashMap<ProductType, List<Product>>();

    public void addProduct(String name, Double price, ProductClass productClass,ProductType productType){
        Product product=new Product(price,name,productType,productClass);
        List<Product> productList=productClassListMap.get(productClass);
        if (productList==null){
            productList=new ArrayList<Product>();
        }
        productList.add(product);
        productClassListMap.put(productClass,productList);
        //dodac tak samo w tej metodzie to mapy wg typu pomoze to w filtrowaniu po typie od razu
    }


}
