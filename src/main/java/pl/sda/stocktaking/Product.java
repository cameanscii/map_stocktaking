package pl.sda.stocktaking;

public class Product {
    double price;
    String name;
    ProductType productType;
    ProductClass productClass;

    public Product(double price, String name, ProductType productType, ProductClass productClass) {
        this.price = price;
        this.name = name;
        this.productType = productType;
        this.productClass = productClass;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ProductClass getProductClass() {
        return productClass;
    }

    public void setProductClass(ProductClass productClass) {
        this.productClass = productClass;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", productType=" + productType +
                ", productClass=" + productClass +
                '}';
    }
}
